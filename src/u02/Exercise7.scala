package u02

object Exercise7 extends App {

  case class Point2D(x: Double, y: Double)

  sealed trait Shape

  object Shape {

    case class Rectangle(leftUpCorner: Point2D, width: Double, height: Double) extends Shape

    case class Square(leftUpCorner: Point2D, sideLength: Double) extends Shape

    case class Circle(center: Point2D, radius: Double) extends Shape

    def perimeter(shape: Shape): Double = shape match {
      case Square(_, l) => l * 4
      case Rectangle(_, w, h) => w * 2 + h * 2
      case Circle(_, r) => r * Math.PI * 2
    }

    def area(shape: Shape): Double = shape match {
      case Square(_, l) => l * l
      case Rectangle(_, w, h) => w * h
      case Circle(_, r) => r * r * Math.PI
    }

  }

}
