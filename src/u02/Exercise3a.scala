package u02

object Exercise3a extends App {

  val parityLambda: Int => String = {
    case x if (x % 2) == 0 => "even"
    case _ => "odd"
  }

  def parityMethod (x: Int): String = x match {
    case x if (x % 2) == 0 => "even"
    case _ => "odd"
  }

  println(parityLambda(2), parityLambda(3))
  println(parityMethod(2), parityMethod(3))

}
