package u02

object Exercise3b extends App {

  val negLambda: (String => Boolean) => (String => Boolean) = {
    (predicate) => !predicate(_)
  }

  def negMethod (predicate: String => Boolean) : String => Boolean = {
    !predicate(_)
  }

  val empty: String => Boolean = _=="" // predicate on strings
  val notEmpty = negLambda(empty) // which type of notEmpty?
  notEmpty("foo")// true
  notEmpty("") // false
  notEmpty("foo") && !notEmpty("") // true.. a comprehensive test


  val notEmptyMethod = negMethod(empty) // which type of notEmpty?
  notEmptyMethod("foo")// true
  notEmptyMethod("") // false
  notEmptyMethod("foo") && !notEmptyMethod("") // true.. a comprehensive test

}
