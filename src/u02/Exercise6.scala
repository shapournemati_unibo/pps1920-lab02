package u02

object Exercise6 extends App {

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case n if (n>= 2) => fib(n - 1) + fib(n - 2)
    case _ => -1 //?
  }

  println(fib(0),fib(1),fib(2),fib(3),fib(4),fib(5),fib(6)) // (0,1,1,2,3,5,8)
  //recursion is NOT tail

}
