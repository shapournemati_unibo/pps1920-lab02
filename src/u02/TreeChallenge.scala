package u02

object TreeChallenge extends App {

  // A custom and generic binary tree of elements of type A
  sealed trait Tree[A]

  object Tree {

    case class Leaf[A](value: A) extends Tree[A]

    case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

    def traverse[A,B](t: Tree[A], branchFun: (B,B) => B, leafFun: (A => B) ) : B  = t match {
      case Branch(l, r) => branchFun(traverse(l,branchFun,leafFun),traverse(r,branchFun,leafFun))
      case Leaf(e) => leafFun(e)
    }

    def size[A](t: Tree[A]): Int = traverse[A, Int](t, _ + _, leaf => 1)
    def find[A](t: Tree[A], elem: A) : Boolean = traverse[A, Boolean](t, _ || _, elem => elem == elem)
    def count[A](t: Tree[A], elem: A) : Int = traverse[A, Int](t, _ + _,
      e => e match {
        case e if(e == elem) => 1
        case _ => 0
      }
    )

  }

  import Tree._

  val tree = Branch(Branch(Leaf(1), Leaf(2)), Leaf(1))
  println(tree, size(tree)) // ..,3
  println(find(tree, 1)) // true
  println(find(tree, 4)) // false
  println(count(tree, 1)) // 2

}
