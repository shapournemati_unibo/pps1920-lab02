package u02

object Exercise4 extends App {

  val p1: Int => (Int => (Int => Boolean)) = {
    (x) => {
      (y) => {
        (z) => {
          x <= y && y <= z
        }
      }
    }
  }

  val p1Mod: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z

  val p2: (Int, Int, Int) => Boolean = {
    def isLessOrEquals: (Int, Int) => Boolean = {
      (x,y) => x <= y
    }
    (x,y,z) => {
      isLessOrEquals(x,z) && isLessOrEquals (y,z)
    }
  }

  def p3(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z
  def p4(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

  println(p1(1)(2)(3), p1(3)(2)(1))
  println(p1Mod(1)(2)(3), p1Mod(3)(2)(1))
  println (p2(1,2,3), p2(3,2,1))
  println(p3(1)(2)(3), p3(3)(2)(1))
  println (p4(1,2,3), p4(3,2,1))



}
