package u02

import u02.Exercise3b.{empty, negMethod}

object Exercise3c extends App {

  def negMethod[A] (predicate: A => Boolean) : A => Boolean = {
    !predicate(_)
  }

  val empty: String => Boolean = _=="" // predicate on strings

  val notEmptyMethod = negMethod(empty) // which type of notEmpty?
  /*println(notEmptyMethod("foo"),// true
    notEmptyMethod(""), // false
    notEmptyMethod("foo") && !notEmptyMethod("") // true.. a comprehensive test
  )*/

  val parityLambda: Int => Boolean = {
    case x if (x % 2) == 0 => true
    case _ => false
  }

  val notParityMethod = negMethod(parityLambda)
  println(notParityMethod(3),// true
    notParityMethod(2), // false
    notParityMethod(3) && !notParityMethod(2) // true.. a comprehensive test
  )

}
