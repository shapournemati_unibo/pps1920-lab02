package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ShapeTest {

  import Exercise7._
  import Exercise7.Shape._

  val square = Square(Point2D(10,10),10)
  val rectangle = Rectangle(Point2D(10,10),5,10)
  val circle = Circle(Point2D(0,0), 10)

  @Test def testSquarePerimeter(): Unit = {
    assertEquals(40, perimeter(square))
  }

  @Test def testSquareArea(): Unit = {
    assertEquals(100, area(square))
  }

  @Test def testRectanglePerimeter(): Unit = {
    assertEquals(30, perimeter(rectangle))
  }

  @Test def testRectangleArea(): Unit = {
    assertEquals(50, area(rectangle))
  }

  @Test def testCirclePerimeter: Unit = {
    assertEquals(62.83185307179586,perimeter(circle),Double.MinPositiveValue)
  }

  @Test def testCircleArea: Unit = {
    assertEquals(314.1592653589793, area(circle),Double.MinPositiveValue)
  }

}
